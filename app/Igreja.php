<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Igreja extends Model
{
    protected $fillable=[
        'id','descricao','endereco'
    ];
}
